//
//  CRCourse.m
//  Curricula
//
//  Created by Brad Ringel on 9/3/13.
//  Copyright (c) 2013 Brad Ringel. All rights reserved.
//

#import "CRCourse.h"
#import "CRAssignment.h"
#import "CRSemester.h"


@implementation CRCourse

@dynamic courseName;
@dynamic creditHours;
@dynamic professorName;
@dynamic assignments;
@dynamic semester;

@end
