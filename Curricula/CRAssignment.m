//
//  CRAssignment.m
//  Curricula
//
//  Created by Brad Ringel on 9/3/13.
//  Copyright (c) 2013 Brad Ringel. All rights reserved.
//

#import "CRAssignment.h"
#import "CRCourse.h"


@implementation CRAssignment

@dynamic category;
@dynamic extraCredit;
@dynamic name;
@dynamic points;
@dynamic pointsOutOf;
@dynamic course;

@end
