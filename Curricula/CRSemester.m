//
//  CRSemester.m
//  Curricula
//
//  Created by Brad Ringel on 9/3/13.
//  Copyright (c) 2013 Brad Ringel. All rights reserved.
//

#import "CRSemester.h"
#import "CRCourse.h"


@implementation CRSemester

@dynamic semesterName;
@dynamic courses;

@end
